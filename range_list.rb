# Task: Implement a class named 'RangeList'
# A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
# A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
# NOTE: Feel free to add any extra member variables/functions you like.

class RangeList
  def initialize
    @range_list = []
    @integer_array = []
  end
  
  def add(range)
    @integer_array += (range.first...range.last).to_a
    update_range_list
  end
  
  def remove(range)
    @integer_array -= (range.first...range.last).to_a
    update_range_list
  end
  
  def print
    puts @range_list.map{|a, b| "[#{a}, #{b})"}.join(" ")
  end

  private
  def update_range_list
    @range_list = []
    @integer_array.sort!.uniq!

    @integer_array.each_cons(2) do |x, y|
      @range_list << [x] if x == @integer_array.first

      unless x + 1 == y
        @range_list.last << x + 1
        @range_list << [y]
      end

      @range_list.last << y + 1 if y == @integer_array.last
    end
  end
end

rl = RangeList.new

rl.add([1, 5])
rl.print
# // Should display: [1, 5)

rl.add([10, 20])
rl.print
# // Should display: [1, 5) [10, 20)

rl.add([20, 20])
rl.print
# // Should display: [1, 5) [10, 20)

rl.add([20, 21])
rl.print
# // Should display: [1, 5) [10, 21)

rl.add([2, 4])
rl.print
# // Should display: [1, 5) [10, 21)

rl.add([3, 8])
rl.print
# // Should display: [1, 8) [10, 21)

rl.remove([10, 10])
rl.print
# // Should display: [1, 8) [10, 21)

rl.remove([10, 11])
rl.print
# // Should display: [1, 8) [11, 21)

rl.remove([15, 17])
rl.print
# // Should display: [1, 8) [11, 15) [17, 21)

rl.remove([3, 19])
rl.print
# // Should display: [1, 3) [19, 21)

rl.remove([3, 19])
rl.print
# // Should display: [1, 3) [19, 21)

rl.remove([2, 3])
rl.print
# // Should display: [1, 2) [19, 21)

rl.remove([2, 20])
rl.print
# // Should display: [1, 2) [20, 21)
